using UnityEngine;

namespace Utils
{
    public static class LayerMaskUtil 
    {
        public static bool LayerMaskContainsLayer(LayerMask mask, int layer) => 
            (mask.value & 1 << layer) > 0;
    }
}
