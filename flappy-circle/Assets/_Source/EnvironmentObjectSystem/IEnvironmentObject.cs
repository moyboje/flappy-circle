﻿using System;
using UnityEngine;

namespace EnvironmentObjectSystem
{
    public interface IEnvironmentObject
    {
        event Action OnDisableObject;
        void SetSpeed(float speed);
        GameObject GetGameObject();
    }
}