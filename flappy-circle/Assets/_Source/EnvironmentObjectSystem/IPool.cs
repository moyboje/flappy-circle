﻿namespace EnvironmentObjectSystem
{
    public interface IPool<T>
    {
        void InitPool();
        void ReturnToPool(T item);
        bool GetItem(out T item);
    }
}