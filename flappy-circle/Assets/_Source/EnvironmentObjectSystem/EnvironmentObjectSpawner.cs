﻿using System.Collections;
using Core;
using UnityEngine;
using Zenject;

namespace EnvironmentObjectSystem
{
    public class EnvironmentObjectSpawner : MonoBehaviour
    {
        [SerializeField] private float obstacleTimeStep;
        [SerializeField] private float bonusTimeStep;
        [SerializeField] private float seed;
        private IPool<IEnvironmentObject> _obstaclePool;
        private IPool<IEnvironmentObject> _bonusPool;

        [Inject]
        public void Construct(
            [Inject(Id = InjectIdData.OBSTACLE_POOL)] IPool<IEnvironmentObject> obstaclePool,
            [Inject(Id = InjectIdData.BONUS_POOL)] IPool<IEnvironmentObject> bonusPool)
        {
            _obstaclePool = obstaclePool;
            _bonusPool = bonusPool;
        }

        public void StartSpawn()
        {
            StartCoroutine(SpawnObstacles());
            StartCoroutine(SpawnBonuses());
        }

        private IEnumerator SpawnObstacles()
        {
            yield return new WaitForSeconds(
                Random.Range(obstacleTimeStep - seed, obstacleTimeStep + seed));

            _obstaclePool.GetItem(out _);
            StartCoroutine(SpawnObstacles());
        }
        
        private IEnumerator SpawnBonuses()
        {
            yield return new WaitForSeconds(
                Random.Range(bonusTimeStep - seed, bonusTimeStep + seed));

            _bonusPool.GetItem(out _);
            StartCoroutine(SpawnBonuses());
        }
    }
}