using System;
using UnityEngine;
using Utils;

namespace EnvironmentObjectSystem
{
    public class Obstacle : MonoBehaviour, IEnvironmentObject
    {
        [SerializeField] private LayerMask borderLayerMask;
        private float _speed;
        public event Action OnDisableObject;

        void Update()
        {
            Move();
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (LayerMaskUtil.LayerMaskContainsLayer(borderLayerMask, col.gameObject.layer))
                gameObject.SetActive(false);
        }

        private void Move() => transform.position += Vector3.left * _speed;
        public void SetSpeed(float speed) => _speed = speed;
        public GameObject GetGameObject() => gameObject;
        
        private void OnDisable()
        {
            OnDisableObject?.Invoke();
        }
    }
}
