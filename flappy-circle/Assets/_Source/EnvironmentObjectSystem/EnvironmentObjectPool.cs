using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace EnvironmentObjectSystem
{
    public abstract class EnvironmentObjectPool<T> : MonoBehaviour, IPool<T> where T : IEnvironmentObject
    {
        [SerializeField] private float movementSpeed; 
        [SerializeField] private List<GameObject> prefabs;
        [SerializeField] private int poolSize;
        private Transform _root;
        private List<T> _pool;
        private List<T> _outOfPool;

        private void Awake()
        {
            _root = GetComponent<Transform>();
        }

        public void InitPool()
        {
            _pool = new();
            _outOfPool = new();

            int prefabId = 0;
            for (int i = 0; i < poolSize; i++)
            {
                var instance = Instantiate(prefabs[prefabId], _root).GetComponent<T>();
                instance.SetSpeed(movementSpeed);
                instance.OnDisableObject += () => ReturnToPool(instance);
                
                ReturnToPool(instance);
                prefabId++;
                if (prefabId >= prefabs.Count)
                    prefabId = 0;
            }
        }

        public virtual void ReturnToPool(T item) 
        {
            if (_outOfPool.Contains(item))
                _outOfPool.Remove(item);
            
            _pool.Add(item);
            item.GetGameObject().SetActive(false);
            item.GetGameObject().transform.localPosition = Vector3.zero;
        }

        public virtual bool GetItem(out T item)
        {
            item = default;
            if (_pool.Count > 0)
            {
                int id = Random.Range(0, _pool.Count);
                item = _pool[id];
                _pool.RemoveAt(id);
                
                _outOfPool.Add(item);
                
                item.GetGameObject().SetActive(true);
                return true;
            }

            return false;
        }
    }
}
