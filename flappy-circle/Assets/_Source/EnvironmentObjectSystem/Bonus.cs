using System;
using UnityEngine;
using Utils;

namespace EnvironmentObjectSystem
{
    public class Bonus : MonoBehaviour, IEnvironmentObject
    {
        [SerializeField] private LayerMask playerLayerMask;
        private float _speed;
        private Transform _target;
        public event Action OnDisableObject;

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (LayerMaskUtil.LayerMaskContainsLayer(playerLayerMask, col.gameObject.layer))
                _target = col.transform;
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (LayerMaskUtil.LayerMaskContainsLayer(playerLayerMask, other.gameObject.layer))
                _target = null;
        }

        private void Update()
        {
            Move();
            MoveToTarget();
        }

        private void Move() => transform.position += Vector3.left * _speed;

        private void MoveToTarget()
        {
            if (_target)
            {
                transform.position = 
                    Vector3.MoveTowards(transform.position, _target.position, _speed);
            }
        }

        public void SetSpeed(float speed) => _speed = speed;
        public GameObject GetGameObject() => gameObject;

        private void OnDisable()
        {
            OnDisableObject?.Invoke();
        }
    }
}
