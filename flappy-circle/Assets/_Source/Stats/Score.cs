using System;

namespace Stats
{
    public class Score
    {
        public int Points { get; private set; }
        public event Action<int> OnPointUpdate = delegate(int currPoints) {  };

        public void AddPoint()
        {
            Points++;
            OnPointUpdate?.Invoke(Points);
        }
    }
}
