using CharacterSystem;
using Core.Services;
using Core.StateSystem;
using EnvironmentObjectSystem;
using InputSystem;
using Stats;
using UnityEngine;
using Zenject;

namespace Core
{
    public class GameInstaller : MonoInstaller
    {
        [SerializeField] private CharacterMovement characterMovement;
        [SerializeField] private ObstaclePool obstaclePool;
        [SerializeField] private BonusPool bonusPool;
        [SerializeField] private EnvironmentObjectSpawner spawner;
        public override void InstallBindings()
        {
            #region Input
            Container.Bind<IInputListener>().To<InputListener>().AsSingle().NonLazy();
            #endregion
            
            #region Environment Pools & Spawn
            Container.Bind<IPool<IEnvironmentObject>>()
                .WithId(InjectIdData.OBSTACLE_POOL)
                .FromInstance(obstaclePool)
                .AsCached().NonLazy();

            Container.Bind<IPool<IEnvironmentObject>>()
                .WithId(InjectIdData.BONUS_POOL)
                .FromInstance(bonusPool)
                .AsCached().NonLazy();
            
            Container.Bind<EnvironmentObjectSpawner>().FromInstance(spawner).AsSingle().NonLazy();
            #endregion

            #region Game States
            Container.Bind<AGameState>()
                .WithId(InjectIdData.PREPARE_STATE_ID)
                .To<PrepareState>()
                .AsSingle().NonLazy();
            
            Container.Bind<AGameState>()
                .WithId(InjectIdData.GAME_STATE_ID)
                .To<MainGameState>()
                .AsSingle().NonLazy();
            
            Container.Bind<AGameState>()
                .WithId(InjectIdData.RESTART_STATE_ID)
                .To<RestartState>()
                .AsSingle().NonLazy();

            Container.Bind<IStateMachine>().To<StateMachine<AGameState>>().AsSingle().NonLazy();
            #endregion

            #region Character
            Container.Bind<CharacterMovement>().FromInstance(characterMovement).AsSingle().NonLazy();
            #endregion

            #region Services
            Container.Bind<ISceneLoader>().To<SceneLoader>().AsSingle().NonLazy();
            #endregion

            #region Stats
            Container.Bind<Score>().AsSingle().NonLazy();
            #endregion

           
        }
    }

    public static class InjectIdData
    {
        public const uint PREPARE_STATE_ID = 0;
        public const uint GAME_STATE_ID = 1;
        public const uint RESTART_STATE_ID = 2;

        public const uint OBSTACLE_POOL = 3;
        public const uint BONUS_POOL = 4;
    }
}