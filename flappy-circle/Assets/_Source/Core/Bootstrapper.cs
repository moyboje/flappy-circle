using Core.StateSystem;
using UnityEngine;
using Zenject;

namespace Core
{
    public class Bootstrapper : MonoBehaviour
    {
        private IStateMachine _stateMachine;
        
        [Inject]
        public void Construct(IStateMachine stateMachine)
        {
            _stateMachine = stateMachine;
        }
        
        void Start()
        {
            _stateMachine.ChangeState<PrepareState>();
        }
    }
}
