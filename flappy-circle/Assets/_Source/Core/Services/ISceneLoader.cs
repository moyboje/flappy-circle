﻿namespace Core.Services
{
    public interface ISceneLoader
    {
        void ReloadScene();
    }
}