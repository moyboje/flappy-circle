using UnityEngine.SceneManagement;

namespace Core.Services
{
    public class SceneLoader : ISceneLoader
    {
        private const int _gameSceneId = 0;
        
        public void ReloadScene() => SceneManager.LoadScene(_gameSceneId);
    }
}