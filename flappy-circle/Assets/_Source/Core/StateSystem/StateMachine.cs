﻿using System;
using System.Collections.Generic;
using Zenject;

namespace Core.StateSystem
{
    public class StateMachine<T> : IStateMachine where T : AGameState
    {
        private readonly Dictionary<Type, T> _states;
        private AGameState _currState;

        [Inject]
        public StateMachine([Inject(Id = InjectIdData.PREPARE_STATE_ID)] T prepareState,
            [Inject(Id = InjectIdData.GAME_STATE_ID)] T gameState,
            [Inject(Id = InjectIdData.RESTART_STATE_ID)] T restartState)
        {
            _states = new Dictionary<Type, T>()
            {
                { typeof(PrepareState), prepareState },
                { typeof(MainGameState), gameState },
                { typeof(RestartState), restartState }
            };
            
            SetupStates();
        }

        private void SetupStates()
        {
            foreach (var state in _states)
                state.Value.Setup(this);
        }

        public bool ChangeState<T>()
        {
            if (_states.ContainsKey(typeof(T)))
            {
                _currState?.Exit();
                _currState = _states[typeof(T)];
                _currState.Enter();
                return true;
            }

            return false;
        }
    }
}