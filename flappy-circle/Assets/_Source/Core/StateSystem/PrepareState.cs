﻿using CharacterSystem;
using EnvironmentObjectSystem;
using InputSystem;
using UnityEngine;
using Zenject;

namespace Core.StateSystem
{
    public class PrepareState : AGameState
    {
        private readonly CharacterMovement _characterMovement;
        private readonly IInputListener _inputListener;
        private readonly IPool<IEnvironmentObject> _obstaclePool;
        private readonly IPool<IEnvironmentObject> _bonusPool;
        
        [Inject]
        public PrepareState(CharacterMovement characterMovement, IInputListener inputListener,
            [Inject(Id = InjectIdData.OBSTACLE_POOL)] IPool<IEnvironmentObject> obstaclePool,
            [Inject(Id = InjectIdData.BONUS_POOL)] IPool<IEnvironmentObject> bonusPool)
        {
            _characterMovement = characterMovement;
            _inputListener = inputListener;
            _obstaclePool = obstaclePool;
            _bonusPool = bonusPool;
        }
        
        public override void Enter()
        {
            _obstaclePool.InitPool();
            _bonusPool.InitPool();
            
            _characterMovement.Prepare();
            
            _inputListener.OnClick += GoToGame;
            _inputListener.ListenClickInput(true);

            
            _inputListener.OnHold += _characterMovement.AddForce;
            _inputListener.ListenHoldInput(true);
        }

        private void GoToGame() => _owner.ChangeState<MainGameState>();

        public override void Exit()
        {
            _inputListener.OnClick -= GoToGame;
        }
    }
}