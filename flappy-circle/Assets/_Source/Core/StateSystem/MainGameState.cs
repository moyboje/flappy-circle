﻿using CharacterSystem;
using EnvironmentObjectSystem;
using Zenject;

namespace Core.StateSystem
{
    public class MainGameState : AGameState
    {
        private readonly CharacterMovement _characterMovement;
        private readonly EnvironmentObjectSpawner _environmentObjectSpawner;
        
        [Inject]
        public MainGameState(CharacterMovement characterMovement, EnvironmentObjectSpawner spawner)
        {
            _characterMovement = characterMovement;
            _environmentObjectSpawner = spawner;
        }

        public override void Enter()
        {
            _characterMovement.Activate();
            _environmentObjectSpawner.StartSpawn();
        }
    }
}