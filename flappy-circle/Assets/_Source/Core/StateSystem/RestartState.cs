﻿using Core.Services;
using Zenject;

namespace Core.StateSystem
{
    public class RestartState : AGameState
    {
        private readonly ISceneLoader _sceneLoader;
        
        [Inject]
        public RestartState(ISceneLoader sceneLoader)
        {
            _sceneLoader = sceneLoader;
        }
        
        public override void Enter()
        {
            _sceneLoader.ReloadScene();
        }
    }
}