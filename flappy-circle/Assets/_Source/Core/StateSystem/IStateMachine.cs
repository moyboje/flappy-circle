﻿namespace Core.StateSystem
{
    public interface IStateMachine
    {
        bool ChangeState<T>();
    }
}