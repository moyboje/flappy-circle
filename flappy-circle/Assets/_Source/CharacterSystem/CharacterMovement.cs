using System;
using UnityEngine;
using UnityEngine.Serialization;
using Utils;

namespace CharacterSystem
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class CharacterMovement : MonoBehaviour
    {
        [SerializeField] private float upForce;
        [SerializeField] private ParticleSystem trail;
        private const float _preparedGravityScale = 0;
        private const float _normalGravityScale = 1;
        private Rigidbody2D _rb;
        private bool _activeForce;
        private void Awake()
        {
            _rb = GetComponent<Rigidbody2D>();
        }

        public void Prepare()
        {
            _rb.gravityScale = _preparedGravityScale;
            trail.Stop();
        }

        public void Activate()
        {
            _rb.gravityScale = _normalGravityScale;
            trail.Play();
        }

        public void AddForce(bool active) => _activeForce = active;

        private void Update()
        {
            if (_activeForce)
                _rb.velocity += Vector2.up * upForce;
        }
    }
}
