using Core;
using Core.StateSystem;
using EnvironmentObjectSystem;
using Stats;
using UnityEngine;
using Utils;
using Zenject;

namespace CharacterSystem
{
    public class CharacterCollision : MonoBehaviour
    {
        [SerializeField] private LayerMask obstacleLayerMask;
        [SerializeField] private LayerMask bonusLayerMask;
        private IStateMachine _stateMachine;
        private Score _score;

        [Inject]
        public void Construct(IStateMachine stateMachine, Score score)
        {
            _stateMachine = stateMachine;
            _score = score;
        }
    
        private void OnCollisionEnter2D(Collision2D col)
        {
            if (LayerMaskUtil.LayerMaskContainsLayer(obstacleLayerMask, col.gameObject.layer))
                _stateMachine.ChangeState<RestartState>();
            else if (LayerMaskUtil.LayerMaskContainsLayer(bonusLayerMask, col.gameObject.layer))
            {
                col.gameObject.SetActive(false);
                _score.AddPoint();
            }
        }
    }
}
