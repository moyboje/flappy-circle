using Stats;
using TMPro;
using UnityEngine;
using Zenject;

namespace UI
{
    public class ScoreView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        private Score _score;

        [Inject]
        public void Construct(Score score)
        {
            _score = score;
        }
        
        private void OnEnable()
        {
            RefreshScoreText(_score.Points);
            _score.OnPointUpdate += RefreshScoreText;
        }

        private void RefreshScoreText(int currScore) => scoreText.text = $"{currScore}";

        private void OnDisable()
        {
            _score.OnPointUpdate -= RefreshScoreText;
        }
    }
}
