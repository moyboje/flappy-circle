using InputSystem;
using UnityEngine;
using Zenject;

namespace UI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class OnTapClose : MonoBehaviour
    {
        private CanvasGroup _cg;
        private IInputListener _inputListener;

        [Inject]
        public void Construct(IInputListener inputListener)
        {
            _inputListener = inputListener;
        }

        private void Awake()
        {
            _inputListener.OnClick += Hide;
        }

        private void Hide()
        {
            _inputListener.OnClick -= Hide;
            gameObject.SetActive(false);
        }
    }
}
