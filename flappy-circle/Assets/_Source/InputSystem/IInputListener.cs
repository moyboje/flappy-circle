﻿using System;

namespace InputSystem
{
    public interface IInputListener
    {
        event Action<bool> OnHold;
        event Action OnClick;
        void ListenHoldInput(bool active);
        void ListenClickInput(bool active);
    }
}