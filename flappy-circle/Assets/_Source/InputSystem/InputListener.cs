using System;
using UnityEngine.InputSystem;
using Zenject;

namespace InputSystem
{
    public class InputListener : IInputListener
    {
        private readonly GameControls _gameControls;
        public event Action<bool> OnHold = delegate(bool active) {  };
        public event Action OnClick = delegate {  };

        [Inject]
        public InputListener()
        {
            _gameControls = new GameControls();
            _gameControls.Enable();
        }


        public void ListenHoldInput(bool active)
        {
            if (active)
                ListenMouseHold();
            else StopListenMouseHold();
        }
        
        public void ListenClickInput(bool active)
        {
            if (active)
                ListenMouseClick();
            else StopListenMouseClick();
        }

        private void ListenMouseHold()
        {
            _gameControls.Game.Click.started += OnMouseClickStarted;
            _gameControls.Game.Click.canceled += OnMouseClickCanceled;
        }
        
        private void ListenMouseClick()
        {
            _gameControls.Game.Click.performed += OnMouseClicked;
        }
        
        private void StopListenMouseHold()
        {
            _gameControls.Game.Click.started -= OnMouseClickStarted;
            _gameControls.Game.Click.canceled -= OnMouseClickCanceled;
        }
        
        private void StopListenMouseClick()
        {
            _gameControls.Game.Click.performed -= OnMouseClicked;
        }

        private void OnMouseClickStarted(InputAction.CallbackContext obj) => OnHold?.Invoke(true);
        private void OnMouseClicked(InputAction.CallbackContext obj)
        {
            OnClick?.Invoke();
        }

        private void OnMouseClickCanceled(InputAction.CallbackContext obj) => OnHold?.Invoke(false);
    }
}
